# STAGE ONE - Build the Source Code
FROM nikolaik/python-nodejs:latest as builder
ARG PUBLIC_URL=/

# Create application directory
WORKDIR /usr/src/app

# Install application dependencies
# (We do this before copying over source files. This prevents unnecesary layer invalidation)
# RUN apk add --no-cache python make
COPY package.json package-lock.json ./

RUN npm ci

# Build application bundle
COPY tsconfig.json ./
COPY src ./src
COPY public ./public

# Build application bundle
RUN npm run build

# STAGE TWO - Run the distributable
FROM node:18-alpine

WORKDIR /usr/dist/app

COPY --from=builder /usr/src/app/build ./

RUN npm install -g serve

EXPOSE 8080
CMD ["serve", "-s", ".", "-l", "8080"]
