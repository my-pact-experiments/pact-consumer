# Pact Consumer

This is built with `create-react-app` so the usual stuff applies:
* `npm start`
* `npm test`

Testing pacts locally, publish against a local broker with (adding in a sha hash)
```
MSYS_NO_PATHCONV=1 docker run --rm \
 -w ${PWD} \
 -v ${PWD}:${PWD} \
 -e PACT_BROKER_BASE_URL=http://host.docker.internal:9292 \
 -e PACT_BROKER_USERNAME=broker \
 -e PACT_BROKER_PASSWORD=test \
 pactfoundation/pact-cli:latest publish ${PWD}/pact/pacts/ \
  --branch=master \
  --consumer-app-version=f1c2697
```