import axios, { AxiosResponse } from "axios";

class QuoteService {
    constructor(readonly baseUrl: string) { }

    getAQuote(): Promise<AxiosResponse> {
        return axios.get(`${this.baseUrl}/quotes`)
    }
}

export default QuoteService;