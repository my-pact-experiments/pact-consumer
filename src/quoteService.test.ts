import { pactWith } from 'jest-pact/dist/v3';
import { MatchersV3 } from '@pact-foundation/pact';
import QuoteService from './quoteService';

pactWith({ consumer: 'pact-consumer', provider: 'pact-provider', cors: true }, interaction => {
  interaction('Quotes API', ({ provider, execute }) => {
    beforeEach(() => provider
      .given('no user')
      .uponReceiving('A request for quotes')
      .withRequest({
        method: 'GET',
        path: '/quotes'
      })
      .willRespondWith({
        status: 404
      })
    );

    execute('not found when there are no quotes', (mockServer) =>
      new QuoteService(mockServer.url)
        .getAQuote()
        .catch(err => {
          expect(err.response.status).toBe(404);
        })
    );
  });

  interaction('Quotes API', ({ provider, execute }) => {
    beforeEach(() => provider
      .given('a comedy-loving user')
      .uponReceiving('A request for quotes')
      .withRequest({
        method: 'GET',
        path: '/quotes'
      })
      .willRespondWith({
        status: 200,
        body: MatchersV3.eachLike({
          quote: "I'm Pickle Rick!",
          author: "Rick Sanchez",
          source: MatchersV3.regex("http(s)?:\/\/([^?#]*)", "https://www.imdb.com/title/tt5218268/")
        })
      })
    );

    execute('receives quotes', (mockServer) =>
      new QuoteService(mockServer.url)
        .getAQuote()
        .then(response => {
          expect(response.status).toBe(200);
          expect(response.data.length).toEqual(1);
        })
    );
  });
});
