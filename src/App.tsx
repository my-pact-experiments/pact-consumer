import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { Quote } from './Quote';
import QuoteService from './quoteService';

const emptyQuotes : Quote[] = [];

function App() {
  const [messages, setMessages] = useState(emptyQuotes);

  useEffect(() => {
    new QuoteService('http://localhost:8081')
      .getAQuote()
      .then(r => {
        setMessages(r.data);
      });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <ul className="App-list">
        {messages.map((m, i) =>
          <li key={i}>
            <h2>{m.author}</h2>
            <p>
              {m.quote}<br />
              <a className="App-link" href={m.source}>Source</a>
            </p>
          </li>
        )}
      </ul>
    </div>
  );
}

export default App;
